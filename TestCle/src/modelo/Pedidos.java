package modelo;

public class Pedidos {
	private int id;
	private int id_user;
	private String tamanho;
	private String recheio;
	private String molho;
	private String massa;
	private int quantidade;
	private float valor;
	public Pedidos(int id, int id_user, String tamanho, String recheio, String molho, String massa, int quantidade, float valor){
		this.setId(id);
		this.setId_user(id_user);
		this.setTamanho(tamanho);
		this.setRecheio(recheio);
		this.setMolho(molho);
		this.setMassa(massa);
		this.setQuantidade(quantidade);
		this.setValor(valor);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getTamanho() {
		return tamanho;
	}
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}
	public String getRecheio() {
		return recheio;
	}
	public void setRecheio(String recheio) {
		this.recheio = recheio;
	}
	public String getMolho() {
		return molho;
	}
	public void setMolho(String molho) {
		this.molho = molho;
	}
	public String getMassa() {
		return massa;
	}
	public void setMassa(String massa) {
		this.massa = massa;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
}
