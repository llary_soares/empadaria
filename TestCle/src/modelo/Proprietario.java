package modelo;

public class Proprietario {
	private int id;
	private String user;
	private String senha;
	public Proprietario(int id, String user, String senha){
		this.setId(id);
		this.setUser(user);
		this.setSenha(senha);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
