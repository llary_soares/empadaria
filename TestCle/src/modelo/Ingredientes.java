package modelo;

public class Ingredientes{
	private int id;
	private String nome, tipo;
	private float preco;
	public Ingredientes(String nome, float preco, String tipo, int id){
		this.setId(id);
		this.setNome(nome);
		this.setTipo(tipo);
		this.setPreco(preco);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	
}
