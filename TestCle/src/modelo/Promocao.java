package modelo;

public class Promocao {
	//private x imagem;
	private int id;
	private String texto;
	public Promocao(int id, String texto){
		this.setId(id);
		this.setTexto(texto);
	}
	public int getId() {
		return id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public void setId(int id) {
		this.id = id;
	}
}
