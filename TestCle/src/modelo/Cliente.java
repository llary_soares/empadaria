package modelo;

public class Cliente {
	private String nome;
	private String user;
	private String senha;
	private int id;
	private String telefone;
	
	public Cliente(String nome, String user, String senha, int id, String telefone){
		this.setNome(nome);
		this.setUser(user);
		this.setSenha(senha);
		this.setId(id);
		this.setTelefone(telefone);
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
