package beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;

import controle.PromocaoControle;
import modelo.Promocao;


@ManagedBean(name="Teste")
public class PromoBean {
	ArrayList<Promocao> lista = new PromocaoControle().consultarTodos();
	public ArrayList<Promocao> getLista(){
		return lista;
	}
	public void setLista(ArrayList<Promocao> lista){
		this.lista = lista;
	}
	public Promocao retornarItem(int id){
		return lista.get(id);
	}
}
