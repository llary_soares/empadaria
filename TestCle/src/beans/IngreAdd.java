package beans;

import javax.faces.bean.ManagedBean;

import controle.IngredientesControle;
import modelo.Ingredientes;

@ManagedBean(name="IngreAdd")
public class IngreAdd {
	private int id;
	private String nome, tipo;
	private float preco;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	public String add() {
		Ingredientes ingre = new Ingredientes(this.getNome(), this.getPreco(), this.getTipo(), this.getId());
		if(new IngredientesControle().add(ingre)) {
			return "index";
		}else {
			return "verIngre";
		}
	}
}
