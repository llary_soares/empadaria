package beans;

import java.sql.SQLException;

import javax.faces.bean.ManagedBean;

import controle.ClienteControle;
import modelo.Cliente;

@ManagedBean(name="ClienteLogar")
public class ClienteLogar {
	private int id;
	private String user;
	private String senha;
	private String nome;
	private String telefone;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String logar() throws SQLException {
		Cliente cliente = new Cliente(this.getNome(), this.getUser(), this.getSenha(), this.getId(), this.getTelefone());
		try {
			if(new ClienteControle().logarCliente(cliente)) {
				return "addPromocao";
			}else {
				return "verIngre";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nome;
	}
}
