package beans;

import javax.faces.bean.ManagedBean;

import controle.IngredientesControle;
import modelo.Ingredientes;


@ManagedBean(name="IngreEdit")
public class IngreEdit {
	private String nome;
	private float preco;
	private String tipo;
	private int id;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String editar() {
		Ingredientes ingre = new Ingredientes(this.getNome(),this.getPreco(),this.getTipo(),this.getId());
		if(new IngredientesControle().editar(ingre)) {
			return "verIngre";	
		}else {
			return "editarIngre";
		}
	}
	public void carregarId(int id) {
		Ingredientes ingre = new IngredientesControle().consultaUm(id);
		this.setId(ingre.getId());
		this.setNome(ingre.getNome());
		this.setPreco(ingre.getPreco());
		this.setTipo(ingre.getTipo());
	}
}
