package beans;

import javax.faces.bean.ManagedBean;

import controle.PromocaoControle;
import modelo.Promocao;

@ManagedBean(name="PromocaoAdd")
public class PromocaoAdd {
	private int id;
	private String texto;
	//private x imagem;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String add() {
		Promocao pro = new Promocao(this.getId(),this.getTexto());
		if(new PromocaoControle().add(pro)) {
			return "index";
		}else {
			return "addUser";
		}
	}
}
