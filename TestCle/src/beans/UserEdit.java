package beans;
import javax.faces.bean.ManagedBean;

import controle.UsuarioControle;
import modelo.Usuario;

@ManagedBean(name="UserEdit")
public class UserEdit {
	private String nome, senha;
	private int id;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String editar() {
		Usuario user = new Usuario(this.getId(),this.getNome(),this.getSenha());
		if(new UsuarioControle().editar(user)) {
			return "index";	
		}else {
			return "editar";
		}
	}
	public void carregarId(int id) {
		Usuario user = new UsuarioControle().consultaUm(id);
		this.setId(user.getId());
		this.setNome(user.getNome());
		this.setSenha(user.getSenha());
	}

	
}