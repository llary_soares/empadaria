package beans;

import javax.faces.bean.ManagedBean;

import controle.PedidosControle;
import modelo.Pedidos;

@ManagedBean(name="PedidoAdd")
public class PedidoAdd {
	private int id;
	private int id_user;
	private String tamanho;
	private String recheio;
	private String molho;
	private String massa;
	private int quantidade;
	private float valor;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getTamanho() {
		return tamanho;
	}
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}
	public String getRecheio() {
		return recheio;
	}
	public void setRecheio(String recheio) {
		this.recheio = recheio;
	}
	public String getMolho() {
		return molho;
	}
	public void setMolho(String molho) {
		this.molho = molho;
	}
	public String getMassa() {
		return massa;
	}
	public void setMassa(String massa) {
		this.massa = massa;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public String add() {
		Pedidos pedido = new Pedidos(this.getId(), this.getId_user(), this.getTamanho(), this.getRecheio(), this.getMolho(), this.getMassa(), this.getQuantidade(), this.getValor());
		if(new PedidosControle().add(pedido)) {
			return "index";
		}else {
			return "verIngre";
		}
	}
}
