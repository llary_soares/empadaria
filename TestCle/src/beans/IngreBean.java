package beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;

import controle.IngredientesControle;
import modelo.Ingredientes;

@ManagedBean(name="IngreBean")
public class IngreBean {
	ArrayList<Ingredientes> lista = new IngredientesControle().consultarTodos();
	public ArrayList<Ingredientes> getLista() {
		return lista;
	}
	
	public void setLista(ArrayList<Ingredientes> lista) {
		this.lista = lista;
	}
	public Ingredientes retornarItem(int id) {
		return lista.get(id);
	}
}
