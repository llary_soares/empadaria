package beans;

import java.sql.SQLException;


import controle.ProprietarioControle;
import modelo.Proprietario;
import javax.faces.bean.ManagedBean;

@ManagedBean(name="ProprietarioLogar")
public class ProprietarioLogar {
	private int id;
	private String user;
	private String senha;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String logar() throws SQLException {
		Proprietario dono = new Proprietario(this.getId(),this.getUser(), this.getSenha());
		if(new ProprietarioControle().logarDono(dono)) {
			return "addPromocao";
		}else {
			return "login";
		}
	}
}
