package beans;

import javax.faces.bean.ManagedBean;

import controle.ClienteControle;
import modelo.Cliente;

@ManagedBean(name="ClienteAdd")
public class ClienteAdd {
	private String nome;
	private String user;
	private String senha;
	private int id;
	private String telefone;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String add() throws Exception {
		Cliente cliente = new Cliente(this.getNome(), this.getUser(), this.getSenha(), this.getId(), this.getTelefone());
		if(new ClienteControle().cadastro(cliente)) {
			return "index";
		}else {
			return "verIngre";
		}
	}
}
