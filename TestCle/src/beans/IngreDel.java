package beans;

import javax.faces.bean.ManagedBean;

import controle.IngredientesControle;

@ManagedBean(name="IngreDel")
public class IngreDel {
	private int id;
	private String nome, tipo;
	private float preco;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	public String deletar(int id) {
		new IngredientesControle().remover(id);
		return "verIngre";
	}
}
