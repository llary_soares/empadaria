package beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;

import controle.PedidosControle;
import modelo.Pedidos;

@ManagedBean(name="PedidoBean")
public class PedidoBean {
	ArrayList<Pedidos> lista = new PedidosControle().consultarTodos();
	public ArrayList<Pedidos> getLista(){
		return lista;
	}
	public void setLista(ArrayList<Pedidos> lista){
		this.lista = lista;
	}
	public Pedidos retornarItem(int id){
		return lista.get(id);
	}
}
