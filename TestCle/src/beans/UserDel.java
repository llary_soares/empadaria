package beans;
import controle.UsuarioControle;
import javax.faces.bean.ManagedBean;
@ManagedBean(name="UserDel")
public class UserDel {
	private int id;
	private String nome, senha;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String deletar(int id) {
		new UsuarioControle().remover(id);
		return "index";
	}

}


