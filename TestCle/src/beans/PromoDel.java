package beans;

import javax.faces.bean.ManagedBean;


import controle.PromocaoControle;

@ManagedBean(name="PromoDel")
public class PromoDel {
	private int id;
	private String texto;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String deletar(int id) {
		new PromocaoControle().remover(id);
		return "verPromocao";
	}
}
