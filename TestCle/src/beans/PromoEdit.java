package beans;

import javax.faces.bean.ManagedBean;

import controle.PromocaoControle;
import modelo.Promocao;

@ManagedBean(name="PromoEdit")
public class PromoEdit {
	private int id;
	private String texto;
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String editar() {
		Promocao promo = new Promocao(this.getId(),this.getTexto());
		if(new PromocaoControle().editar(promo)) {
			return "verPromocao";	
		}else {
			return "editarPromo";
		}
	}
	public void carregarId(int id) {
		Promocao promo = new PromocaoControle().consultaUm(id);
		this.setId(promo.getId());
		this.setTexto(promo.getTexto());
	}
}
