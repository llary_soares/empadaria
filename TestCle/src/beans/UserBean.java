package beans;
import javax.faces.bean.ManagedBean;
import controle.UsuarioControle;
import modelo.Usuario;
import java.util.ArrayList;
// Dando nome ao javaBean
@ManagedBean(name="UserBean")

public class UserBean {
	// Lista dos usuários que obtida através da classe controle
	ArrayList<Usuario> lista = new UsuarioControle().consultarTodos();
	public ArrayList<Usuario> getLista() {
		return lista;
	}
	
	public void setLista(ArrayList<Usuario> lista) {
		this.lista = lista;
	}
	public Usuario retornarItem(int id) {
		return lista.get(id);
	}
}