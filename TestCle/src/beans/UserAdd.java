package beans;

import javax.faces.bean.ManagedBean;
import modelo.Usuario;
import controle.UsuarioControle;

@ManagedBean(name="UserAdd")
public class UserAdd {
	int id;
	String nome, senha;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String add() {
		Usuario user = new Usuario(this.getId(),this.getNome(), this.getSenha());
		if(new UsuarioControle().add(user)) {
			return "index";
		}else {
			return "addUser";
		}
	}

}



