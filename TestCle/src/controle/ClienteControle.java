package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Cliente;
import controle.Crypt;

public class ClienteControle {
	public boolean cadastro(Cliente cliente) throws Exception{
		Crypt crip = new Crypt();
		String chave = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDL3zlyBF/j6AfR\r\n" + 
				"9IwKAtZ6B5tq41b0L8dqm8FOycDNuHWGChg7CPNWrn44PVMsOQpy6SD532rqx/5A\r\n" + 
				"M36ieylS+jrADwk9PW258qTMZPMyx1DEJG2PiGfs+mpMD03lycRuGmLEKQeInIS9\r\n" + 
				"TClOmFFPeTnSkTeDkqX5y1A3TOH1FEXoGgKOpDWXZvGITlkl+kH4fwcoagLsIsek\r\n" + 
				"yK88byCw6r4NLH22MeQD9LxEXuihe0HqaMTu8CPSeBljVdEjkmYx6pp4UAfuqnn8\r\n" + 
				"zIuQFNUhuw1vJepj4pXIKsitoEXa918GuPetXjCtA1oAIzoUOF5tSpsPAoY44JJV\r\n" + 
				"es13rVhfAgMBAAECggEADAci8f2dFKqbT4FAg9SwB6oMOs2n0ydAeiMvT/EqPVjd\r\n" + 
				"IifVTyJTjPElhDbmAc1ptubXKbbLLYfYEbyYA4kFop4dujgI4QKPRzGwFFj+WigV\r\n" + 
				"NUU664VuMDaD7/HVNDHns2E+I3mSNraZRDvKkhb9cRVjWm9z2YDc5vReSqzwBc/t\r\n" + 
				"4SG7j2cBOicOd+dOZh7DfsMWORERpcdQjazQf1ArdXV2GwuBPRdz1BfgvrZmNj9o\r\n" + 
				"6/WjVQV+MtMnX7HAmsw2s8jUCYO3lfx7I0/m/EIOFLfbuAgP4y+bVhDyKlQnQLOg\r\n" + 
				"VrZmiYqB05JcqGsw51Df7sK8kKzBrTi+FOtANb7dpQKBgQDyPsQEBFe+tVKDUMGo\r\n" + 
				"+LytXbheENbcq4t1C7c+ZmK7wzDwTdLOhdw8QcDyif3bm2NZcBNqZSYegWoFLn+2\r\n" + 
				"H/SzntAIvAGzzE8Z86FeEKQu4PnfUuxpNa6oDaTdLgBBAJoRTD8wzsCMkwET8sGp\r\n" + 
				"6ge6Whzz4r83NO8mz+q/R94bYwKBgQDXcqwe13Y78i7fkhs3vv2UxLbnUbAJKcjK\r\n" + 
				"3l2JlxJmvamtZ1Ew0jRsXYzAYvCIS+/GFy4r2YzlBLEbd2bi7l/sgh50UNSpXaSx\r\n" + 
				"uCARYZOm5Z70LxvhkMGmLXHPV1kzxa6Sy72XmYXhaBc/Ns4orbJ+sMeJOAbm72Po\r\n" + 
				"4RNfuWvl1QKBgDsPRmbcUDA0sNtHExAJJKb31H1KibffMu7kXlaeS7APVJ0hvCWR\r\n" + 
				"yTH/rfTz46po5f3mLzWfV33Ue26r+YMDo3svWvTmMVwOkbJ4DX2LfRvYydLCutSj\r\n" + 
				"u+NJAErUbkdqyCUze6yAm70qEfc1FjZA0oWCdtCXFZt2EmBaDJd6BBKVAoGBAME0\r\n" + 
				"iZvi1pmtdlFxwcy9DsShn/BS9g1RlkovHSys+IiAHzBszYd9iht/zSAd2dwwVOaM\r\n" + 
				"lRAnuM0L5xNdgTuSTx1WFp9yeTMk0fO5zbAok/OASYpq0JL4cGBosn4gs9LUvNfR\r\n" + 
				"s8TGnSPlZ6t9p2UdV0t7loS8ZJwmI6+MYAZgzpy9AoGBAIsrLcu8B0O0SWbtPH/C\r\n" + 
				"jtq4YFt79/aSbCr6JtRQE6Yvs2RaqxcSe7coEugz9weMl3oOqVaYe5zIjUQBxdkh\r\n" + 
				"+xulA5lcLblZMyMCplo3debzEZ+tIN736r5bmQjEzq+PeIyQgtleoXwjfpEHyQMD\r\n" + 
				"eszFTW8/Jff2F4lVO/5J8xR6";
		byte[] resposta;
		resposta = crip.encrypt(cliente.getSenha(), chave);
		System.out.println(resposta);
		boolean resultado = false;	
		try {
			Connection con = new Conexao().abrirConexao();
			boolean aux = this.selecionarUser(cliente.getUser());
			if(aux) {
				String sql = "INSERT INTO cliente(nome, user, senha, telefone) VALUES(?,?,?,?);";	
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, cliente.getNome());
				ps.setString(2, cliente.getUser());
				ps.setBytes(3, resposta);
				ps.setString(4, cliente.getTelefone());
				if(!ps.execute()) {
					resultado = true;		
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());	
		}
		return resultado;	
	}
	public boolean selecionarUser(String user) throws SQLException{
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();	
			try {	
				String sql = "SELECT count(user) as abc FROM cliente WHERE user=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, user);
				ResultSet rs = ps.executeQuery();
				rs.next();
				if(rs.getInt("abc") == 0){
					resultado = true;			
				}
			
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}finally {
				new Conexao().fecharConexao(con);
			}
		return resultado;
	}
	public boolean logarCliente(Cliente cliente) throws Exception{
		System.out.println("antes do try");
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
			try {	
				System.out.println("dentro try");
				String sql = "SELECT * FROM cliente WHERE user=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, cliente.getUser());
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()) {
					System.out.println("dentro if");
					Crypt crip = new Crypt();
					String chave = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDL3zlyBF/j6AfR\r\n" + 
							"9IwKAtZ6B5tq41b0L8dqm8FOycDNuHWGChg7CPNWrn44PVMsOQpy6SD532rqx/5A\r\n" + 
							"M36ieylS+jrADwk9PW258qTMZPMyx1DEJG2PiGfs+mpMD03lycRuGmLEKQeInIS9\r\n" + 
							"TClOmFFPeTnSkTeDkqX5y1A3TOH1FEXoGgKOpDWXZvGITlkl+kH4fwcoagLsIsek\r\n" + 
							"yK88byCw6r4NLH22MeQD9LxEXuihe0HqaMTu8CPSeBljVdEjkmYx6pp4UAfuqnn8\r\n" + 
							"zIuQFNUhuw1vJepj4pXIKsitoEXa918GuPetXjCtA1oAIzoUOF5tSpsPAoY44JJV\r\n" + 
							"es13rVhfAgMBAAECggEADAci8f2dFKqbT4FAg9SwB6oMOs2n0ydAeiMvT/EqPVjd\r\n" + 
							"IifVTyJTjPElhDbmAc1ptubXKbbLLYfYEbyYA4kFop4dujgI4QKPRzGwFFj+WigV\r\n" + 
							"NUU664VuMDaD7/HVNDHns2E+I3mSNraZRDvKkhb9cRVjWm9z2YDc5vReSqzwBc/t\r\n" + 
							"4SG7j2cBOicOd+dOZh7DfsMWORERpcdQjazQf1ArdXV2GwuBPRdz1BfgvrZmNj9o\r\n" + 
							"6/WjVQV+MtMnX7HAmsw2s8jUCYO3lfx7I0/m/EIOFLfbuAgP4y+bVhDyKlQnQLOg\r\n" + 
							"VrZmiYqB05JcqGsw51Df7sK8kKzBrTi+FOtANb7dpQKBgQDyPsQEBFe+tVKDUMGo\r\n" + 
							"+LytXbheENbcq4t1C7c+ZmK7wzDwTdLOhdw8QcDyif3bm2NZcBNqZSYegWoFLn+2\r\n" + 
							"H/SzntAIvAGzzE8Z86FeEKQu4PnfUuxpNa6oDaTdLgBBAJoRTD8wzsCMkwET8sGp\r\n" + 
							"6ge6Whzz4r83NO8mz+q/R94bYwKBgQDXcqwe13Y78i7fkhs3vv2UxLbnUbAJKcjK\r\n" + 
							"3l2JlxJmvamtZ1Ew0jRsXYzAYvCIS+/GFy4r2YzlBLEbd2bi7l/sgh50UNSpXaSx\r\n" + 
							"uCARYZOm5Z70LxvhkMGmLXHPV1kzxa6Sy72XmYXhaBc/Ns4orbJ+sMeJOAbm72Po\r\n" + 
							"4RNfuWvl1QKBgDsPRmbcUDA0sNtHExAJJKb31H1KibffMu7kXlaeS7APVJ0hvCWR\r\n" + 
							"yTH/rfTz46po5f3mLzWfV33Ue26r+YMDo3svWvTmMVwOkbJ4DX2LfRvYydLCutSj\r\n" + 
							"u+NJAErUbkdqyCUze6yAm70qEfc1FjZA0oWCdtCXFZt2EmBaDJd6BBKVAoGBAME0\r\n" + 
							"iZvi1pmtdlFxwcy9DsShn/BS9g1RlkovHSys+IiAHzBszYd9iht/zSAd2dwwVOaM\r\n" + 
							"lRAnuM0L5xNdgTuSTx1WFp9yeTMk0fO5zbAok/OASYpq0JL4cGBosn4gs9LUvNfR\r\n" + 
							"s8TGnSPlZ6t9p2UdV0t7loS8ZJwmI6+MYAZgzpy9AoGBAIsrLcu8B0O0SWbtPH/C\r\n" + 
							"jtq4YFt79/aSbCr6JtRQE6Yvs2RaqxcSe7coEugz9weMl3oOqVaYe5zIjUQBxdkh\r\n" + 
							"+xulA5lcLblZMyMCplo3debzEZ+tIN736r5bmQjEzq+PeIyQgtleoXwjfpEHyQMD\r\n" + 
							"eszFTW8/Jff2F4lVO/5J8xR6";
					byte [] senha = rs.getBytes("senha");
					System.out.println("dentro do if 2");
					String pwd = crip.decrypt(senha, chave);
					//System.out.println("senha: "+senha);
					System.out.println("pwd: "+pwd);
					if(pwd.equals(cliente.getSenha())) {
						resultado = true;
					}			
				}
			
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}finally {
				new Conexao().fecharConexao(con);
			}
		return resultado;
	}
	public boolean editar(Cliente user) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE cliente SET nome=?, user=?, senha=?, telefone=? WHERE id=?");
			ps.setString(1,user.getNome());
			ps.setString(2,user.getUser());
			ps.setString(3, user.getSenha());
			ps.setString(4,user.getTelefone());
			ps.setInt(5, user.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o usu�rio: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}

	public Cliente consultaUm(int id) {
		Cliente user = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM cliente WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				if(rs.next()) {
					user = new Cliente(rs.getString("nome"),rs.getString("user"),rs.getString("senha"),rs.getInt("id"),rs.getString("telefone"));		
			
				}
			}
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return user;
	}
}
