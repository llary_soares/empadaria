package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import modelo.Proprietario;

public class ProprietarioControle {
	public boolean logarDono(Proprietario dono) throws SQLException{
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
			try {	
				String sql = "SELECT * FROM proprietario WHERE user=? and senha=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, dono.getUser());
				ps.setString(2, dono.getSenha());
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()) {
					resultado = true;			
				}
			
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}finally {
				new Conexao().fecharConexao(con);
			}
		return resultado;
	}
}
