package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Pedidos;

public class PedidosControle {
	public boolean add(Pedidos pedido) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("Insert INTO pedidos(id_user, tamanho, recheio, molho, massa, quantidade, valor) VALUES (?,?,?,?,?,?,?)");
			ps.setInt(1, pedido.getId());
			ps.setString(2, pedido.getTamanho());
			ps.setString(3, pedido.getRecheio());
			ps.setString(4, pedido.getMolho());
			ps.setString(5, pedido.getMassa());
			ps.setInt(6, pedido.getQuantidade());
			float precoT = this.selecionarTamanho(pedido.getTamanho());
			float precoR = this.selecionarRecheio(pedido.getRecheio());
			float precoMolho = this.selecionarMolho(pedido.getMolho());
			float precoM = this.selecionarMassa(pedido.getMassa());
			float valor = pedido.getQuantidade() * (precoT + precoR + precoMolho + precoM);
			ps.setFloat(7, valor);
			System.out.println(pedido.getQuantidade());
			System.out.println(precoT);
			System.out.println(precoR);
			System.out.println(precoMolho);
			System.out.println(precoM);
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao adicionar registro." + e.getMessage());
		}
		return resultado;
	}
	
	public float selecionarTamanho(String tamanho) throws SQLException{
		float resultado = 0;
		Connection con = new Conexao().abrirConexao();	
			try {	
				String sql = "SELECT preco FROM ingredientes WHERE nome=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, tamanho);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					resultado = rs.getFloat("preco");			
				}
			
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}finally {
				new Conexao().fecharConexao(con);
			}
		System.out.println(resultado);
		return resultado;
	}
	
	public float selecionarRecheio(String recheio) throws SQLException{
		float resultado = 0;
		Connection con = new Conexao().abrirConexao();	
			try {	
				System.out.println(recheio);
				String sql = "SELECT preco FROM ingredientes WHERE nome=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, recheio);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					resultado = rs.getFloat("preco");			
				}
			
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}finally {
				new Conexao().fecharConexao(con);
			}
		return resultado;
	}
	
	public float selecionarMolho(String molho) throws SQLException{
		float resultado = 0;
		Connection con = new Conexao().abrirConexao();	
			try {	
				String sql = "SELECT preco FROM ingredientes WHERE nome=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, molho);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					resultado = rs.getFloat("preco");			
				}
			
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}finally {
				new Conexao().fecharConexao(con);
			}
		return resultado;
	}
	
	public float selecionarMassa(String massa) throws SQLException{
		float resultado = 0;
		Connection con = new Conexao().abrirConexao();	
			try {	
				String sql = "SELECT preco FROM ingredientes WHERE nome=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, massa);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					resultado = rs.getFloat("preco");			
				}
			
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}finally {
				new Conexao().fecharConexao(con);
			}
		return resultado;
	}
	public ArrayList<Pedidos> consultarTodos(){
		ArrayList<Pedidos> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM pedidos;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Pedidos>();
				while(rs.next()) {
					Pedidos pedido = new Pedidos(rs.getInt("id"),rs.getInt("id_user"),rs.getString("tamanho"),rs.getString("recheio"),rs.getString("molho"),rs.getString("massa"),rs.getInt("quantidade"),rs.getFloat("valor"));
					lista.add(pedido);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	public int selecionarId_user(int id) throws SQLException{
		int resultado = 0;
		Connection con = new Conexao().abrirConexao();	
			try {	
				String sql = "SELECT id_user FROM pedidos WHERE id=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setInt(1, id);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					resultado = rs.getInt("id_user");			
				}
			
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}finally {
				new Conexao().fecharConexao(con);
			}
		System.out.println(resultado);
		return resultado;
	}
	public boolean editar(Pedidos p) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			int id_user = this.selecionarId_user(p.getId());
			PreparedStatement ps = con.prepareStatement("UPDATE pedidos SET id_user=?,tamanho=?,recheio=?,molho=?,massa=?,quantidade=?,valor=? WHERE id=?");
			ps.setInt(1,id_user);
			ps.setString(2, p.getTamanho());
			ps.setString(3, p.getRecheio());
			ps.setString(4, p.getMolho());
			ps.setString(5, p.getMassa());
			ps.setInt(6, p.getQuantidade());
			float precoT = this.selecionarTamanho(p.getTamanho());
			float precoR = this.selecionarRecheio(p.getRecheio());
			float precoMolho = this.selecionarMolho(p.getMolho());
			float precoM = this.selecionarMassa(p.getMassa());
			float valor = p.getQuantidade() * (precoT + precoR + precoMolho + precoM);
			ps.setFloat(7,valor);
			ps.setInt(8, p.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o usu�rio: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}

	public Pedidos consultaUm(int id) {
		Pedidos p = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM pedidos WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				if(rs.next()) {
					p = new Pedidos(rs.getInt("id"),rs.getInt("id_user"),rs.getString("tamanho"),rs.getString("recheio"),rs.getString("molho"),rs.getString("massa"),rs.getInt("quantidade"),rs.getFloat("valor"));		
			
				}
			}
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return p;
	}
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM pedidos WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
}
