package controle;
import modelo.Usuario;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
// Classe de controle como sempre, nada de especial aqui.
public class UsuarioControle {
	public boolean add(Usuario user) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("Insert INTO usuario(nome,senha) VALUES (?,?)");
			ps.setString(1, user.getNome());
			ps.setString(2, user.getSenha());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao adicionar registro." + e.getMessage());
		}
		return resultado;
	}
	public ArrayList<Usuario> consultarTodos(){
		ArrayList<Usuario> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Usuario>();
				while(rs.next()) {
					Usuario user = new Usuario(rs.getInt("id"),rs.getString("nome"),rs.getString("senha"));
					lista.add(user);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean editar(Usuario user) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE usuario SET nome=?, senha=? WHERE id=?");
			ps.setString(1,user.getNome());
			ps.setString(2, user.getSenha());
			ps.setInt(3, user.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o usu�rio: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}

	public Usuario consultaUm(int id) {
		Usuario user = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				if(rs.next()) {
					user = new Usuario(rs.getInt("id"),rs.getString("nome"),rs.getString("senha"));		
			
				}
			}
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return user;
	}
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM usuario WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}


}
