package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Ingredientes;

public class IngredientesControle {
	public boolean add(Ingredientes ingre) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("Insert INTO ingredientes(nome,preco,tipo) VALUES (?,?,?)");
			ps.setString(1, ingre.getNome());
			ps.setFloat(2, ingre.getPreco());
			ps.setString(3, ingre.getTipo());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao adicionar registro." + e.getMessage());
		}
		return resultado;
	}
	
	public ArrayList<Ingredientes> consultarTodos(){
		ArrayList<Ingredientes> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM ingredientes;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Ingredientes>();
				while(rs.next()) {
					Ingredientes ingre = new Ingredientes(rs.getString("nome"),rs.getFloat("preco"),rs.getString("tipo"),rs.getInt("id"));
					lista.add(ingre);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	public boolean editar(Ingredientes ingre) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE ingredientes SET nome=?, preco=?, tipo=? WHERE id=?");
			ps.setString(1,ingre.getNome());
			ps.setFloat(2, ingre.getPreco());
			ps.setString(3, ingre.getTipo());
			ps.setInt(4, ingre.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar o usu�rio: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}

	public Ingredientes consultaUm(int id) {
		Ingredientes ingre = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM ingredientes WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				if(rs.next()) {
					ingre = new Ingredientes(rs.getString("nome"),rs.getFloat("preco"),rs.getString("tipo"),rs.getInt("id"));		
			
				}
			}
		new Conexao().fecharConexao(con);
	}catch(SQLException e) {
		System.out.println("Erro no servidor: " + e.getMessage());
	}
		
		return ingre;
	}
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM ingredientes WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
}
